# webscrape.rb
#
# Brad Stewart
# The core logic of the wbscrape functionality. 

require_relative 'lib/scraper'
require_relative 'lib/xlsx'
require_relative 'lib/utils'
require_relative 'lib/digikey'

require 'rubygems'
require 'bundler/setup'
require 'optparse'
require 'yaml'
require 'active_support/hash_with_indifferent_access'
require 'active_support/core_ext/array'

## Load configuration files
@params = YAML.load_file(ARGV[0])
# @c = YAML.load_file('config/digikey.yml')
@time = Time.now.strftime("%m-%d-%Y__%H-%M")

# $stderr.reopen('logs/log.txt', 'w')

categories = @params['category'].split(";").map( &:strip ) unless @params['category'].nil?
flags = @params['flags'].split(" ").map( &:strip ) unless @params['flags'].nil?
# keywords = @params['keywords'].split(";").map( &:strip )

# Get a new generic WebScraper and add Digikey-specific methods to the 
#   WebScraper object. The params file should be used to specificy which 
#   set of methods are extended if more websites (e.g. Mouser) are added.
driver = WebScraper.new('http://www.digikey.com', @params ) 
driver.extend(Digikey) 

## Parse params and collect the data
if @params.key?('url')
	driver.goto( @params['url'] )
	driver.scrape
else
	# @params['parametric'] = false if @params['parametric'].nil? #set default
	# op = @params['parametric'] == true ? ' .and. ' : ' .or. '
	searchStrings = Digikey.create_query_strings( @params )

	categories.each do |category|
		searchStrings.each do |query| 

			driver.reset
			driver.scrape( { query: query, category: category } )

		end
	end
end

## Write data to disk

FileUtils.makedirs @params['directory'] # Create non-existent directories
# Generate filename and write formatted data to xlsx file
xlsxFile = Utils.rm_file_ext( @params['filename'] ) + '_' + @time + '.xlsx'
xls = Excel.new
xls.add_sheet( @time, driver.data )

@params['scheduled'] = false if @params['scheduled'].nil? #set default value

if @params.key?('scheduled') and @params['scheduled'] == true
	#Look for price changes or new products
	diff = Utils.hash_diff( @params['previous'].last, driver.data ) unless @params['previous'].nil?
	xls.sheet( 'Changes_on_'+@time, diff )

	# Generate filename and write data to json file
	FileUtils.makedirs @params['directory']+'/json'
	dataFile = @params['directory']+'/json/' + Utils.rm_file_ext( @params['filename'] ) + '_' + Utils.sanitize_fname( @params['category'] ) +'_'+ @time + '.json'
	File.open( dataFile, 'w') { |f| f.write( driver.data.to_json )}

	# Update previous json data reference in parameters file
	( @params['previous'] ||= [] ) << Utils.windows_path( dataFile )
	File.open(ARGV[0], 'w') { |f| YAML.dump(@params, f) }
end

xls.write_file( @params['directory'] +'/'+ xlsxFile )
driver.close
