# Webscrape

Ruby script to retrieve product listings, price, and feature information from Digikey.com.

## Installation

Ensure you have Ruby installed. Get it from http://rubyinstaller.org/downloads/.
Download the source code/clone this repository. You also need Firefox installed.

Install dependencies by executing:
```
$ cd /path/to/webscrape-ruby
$ bundle install
```
## Configuration
The scraper is configured via a YAML file passed as the first (and only) command-line argument. A sample, params.yml, is provided. Configuration options are detailed below.

#### Standard
```yaml
keywords: C8051F990; EFM32TG                # Search terms, semi-colon separated
category: Embedded - Microcontrollers       # Digikey product category
filename: silabs                            # Output filename
directory: C:\dev\ws-test\                  # Output directory
```
Do not include "*" or other wildcard characters, simply truncate the search term at the desired place. The category field must exactly match the Digikey category you wish to search. Separate keywords with semi-colons (";"). By default, each ";" is replaced with "OR". For simple parametric searches (e.g. "AND"), include the following line:
```
parametric: true
```
Firefox is the default browser, but you can specify a different one by including the following option in your params file:
```
browser: phantomjs                          # Phantomjs is a browser name
```
I have only used Firefox and the PhantomJS ghost driver, but other browsers are supported. If you want to use PhantomJS (runs the scraper without a viewable browser), download it from http://phantomjs.org/download.html. Extract the ZIP and copy phantomjs.exe from .\bin to your Ruby installation's .\bin folder (or some other directory included in your PATH).
 
#### Advanced Parametric Search
For more advanced parametric searches, a parametrized Digikey URL may be provided inplace of "keyword" and "category":
```
url: http://www.digikey.com/....
```
Either "keywords" and "category", or "url" must be provided. You should not provide both.
#### Scheduled
The code also provides functions for comparing data from the same search performed at different times. If you plan to schedule searches to run periodically and detect price drops, new products, or other changes, include:
```
scheduled: true
```
The script will now save JSON representations of the data after each run and store references to those files in the supplied YAML parameter file. For example, you might see this appear after a two runs:
```
previous:
- C:\dev\ws-test\json\silabs_Embedded___Microcontrollers_11-10-2013__18-03.json
- C:\dev\ws-test\json\silabs_Embedded___Microcontrollers_12-10-2013__18-15.json
```
Additionally, the generated Excel file will contain another sheet detailing any detected changes.

## Usage
To perform a scraping operation:
```
$ ruby webscrape.rb params.yml
```
