# Utils.rb
#
# Brad Stewart
# Module with various static methods

require 'rubygems'
require 'json'
require 'csv'
require 'pp'
require 'active_support/core_ext/hash'

module Utils
	def self.hash_diff(oldDataFile, newData)
		oldData = JSON.load( File.read(File.expand_path(oldDataFile) ) )
		d = oldData.diff( newData )
		d2 = newData.diff( oldData )
		# pp d2
		# newPart = {}
		# changedPart = {}

		# d2.keys.each do |key|
		# 	if d[key].nil?
		# 		newPart[key] == d2[key]
		# 	else
		# 		changedPart[key] == d2[key]
		# 	end
		# end

		# CSV.open('test_diff_file.csv', 'wb') { |csv| d2.to_a.each { |elem| csv << elem.flatten } }

	end

	def self.rm_file_ext( fileName )
		str = nil
		if fileName.include? '.'
			str = fileName[0, fileName.rindex('.')]
		else
			str = fileName
		end
	end

	# Build array of < 256 character query strings with the required boolean operator
	def self.create_queries( array = [], options = {} )
		options[:operator] = ' .or. ' if options[:operator].nil?
		searchStrings = []
		t = array[0]

		array.from(1).each do |e|
			if (t.size + options[:operator].size + e.size) < 256
				t << ( options[:operator] + e )
			else
				searchStrings << t
				t = e
			end
		end
		return searchStrings << t
	end

	def self.windows_path( s )
	  s.gsub('/', '\\')
	end

	def self.sanitize_fname( s )
		s.gsub(/[^\w\.]/, '_')
	end

end