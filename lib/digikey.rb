# digikey.rb
#
# Brad Stewart
# Module providing Digikey specific helper methods and web control functions.
# Should be extended by any instance of WebScraper looking at Digikey.

module Digikey

	extend self
	# Build array of < 256 character query strings with the required boolean operator
	def create_query_strings( p = {} )
		keywords = p['keywords'].split(";").map( &:strip )
		p['parametric'] = false if p['parametric'].nil? #set default
		op = p['parametric'] == true ? ' .and. ' : ' .or. '
				
		searchStrings = []
		t = keywords[0]

		keywords.from(1).each do |e|
			if (t.size + options[:operator].size + e.size) < 256
				t << ( options[:operator] + e )
			else
				searchStrings << t
				t = e
			end
		end
		return searchStrings << t
	end

	def scrape( options = {} )
		if options.key?(:query) and options.key?(:category)
			enter_params( options[:query], options[:category] ) 
		end
		enter_quantity( '100' )		
		read_table
		read_price( '1000', '1Ku' )
		read_price( '10000', '10Ku' )
	end

	private

		def enter_params( param, category )	
			@browser.text_field( :name => 'KeyWords').set( param )
			@browser.checkbox( :name => 'rohs' ).set
			@browser.form( :name => 'Sform' ).submit
			@browser.link( :text => category ).click
		end

		def enter_quantity( qty )
			if @browser.link( :text => 'First' ).exists?
				@browser.link( :text => 'First' ).click
			end
			@browser.text_field( :name => 'quantity' ).set ( qty )
			@browser.button( :value => 'Submit' ).click
		end

		def read_table()
			table = @browser.table( :id => 'productTable' ).to_a

			while @browser.link( :text => 'Next' ).exists? do
				@browser.link( :text => 'Next' ).click
				table += @browser.table( :id => 'productTable' ).to_a
			end
			len = table[0].length-1

			digikeyPartNumberColNum = 3
			hash = table.inject({}) { |hash,element| 
				hash[element[digikeyPartNumberColNum]] = element[digikeyPartNumberColNum+1..len]
				hash
			}
			@data.merge!( hash )
			#return hash
		end

		# Expects a hash populated by readTable, then adds another price
		def read_price( qty, unit )
			# Add to the header row
			(@data['Digi-Key Part Number'] ||= []) << "Price @ #{unit}"
			enter_quantity( qty )
			# get number of results returned by digikey
			count = @browser.p( :class => 'matching-records' ).text.scan(/\d+/).first.to_i
			count -= 1 #the table is 0-indexed

			while true
				# digikey returns 25 results per page
				index = ( count > 24 ) ? 24 : count 
				0.upto(index) do |i|
					pn = @browser.td( :class => 'digikey-partnumber', :index => i ).text
					price = @browser.td( :class => 'unitprice', :index => i ).text
					( @data[pn] ||= [] ) << price
				end

				if @browser.link( :text => 'Next' ).exists?
					@browser.link( :text => 'Next' ).click
					count -= 25 # This needs to be 25 as each page is 0 indexed
				else
					break
				end
			end 
		end

end