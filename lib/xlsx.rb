require 'axlsx'

class Excel
	@wb
	@fileName
	@p

	def initialize()
		Axlsx::Package.new do |p|
			@p = p
			@wb = p.workbook
		end
	end

	def add_sheet( name, data )
		@wb.styles do |s|
			silabs = s.add_style :bg_color => 'BDBDBD', :b => true		

			#create and populate worksheet
			@wb.add_worksheet(:name => name) do |sheet|

				# get the index of the manufacturer column
				mCol = data['Digi-Key Part Number'].index('Manufacturer')

				data.each do |key, array|
					if ( array[mCol] == 'Silicon Laboratories Inc' )
						sheet.add_row array, :style => silabs
					elsif key != ''
						sheet.add_row array
					end
				end

				# enable table filters
				range = "A1:#{sheet.cells.last.r}"
				sheet.auto_filter = range

				# freeze the first column and row
				sheet.sheet_view.pane do |pane|
					pane.state = :frozen
					pane.x_split = 1
					pane.y_split = 1
				end
			end
		end
	end

	def write_file( name )
		@p.serialize( name )
	end

end

def testXls( data )
	Axlsx::Package.new do |p|
		wb = p.workbook
		# setup custom styles
		wb.styles do |s|
			silabs = s.add_style :bg_color => 'BDBDBD', :b => true		

			#create and populate worksheet
			@wb.add_worksheet(:name => Time.now.strftime("%d-%m-%Y__%H-%M")) do |sheet|

				# get the index of the manufacturer column
				mCol = data['Digi-Key Part Number'].index('Manufacturer')

				data.each do |key, array|
					if ( array[mCol] == 'Silicon Laboratories Inc' )
						sheet.add_row array, :style => silabs
					else
						sheet.add_row array
					end
				end
			end
		end
	end
	
end

