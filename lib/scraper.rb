# scraper.rb
#
# This file contains the code for the WebScrape class which controls the web
#   driver which interacts with Digikey and/or other websites.
# Functions which issue commands to or retrieve data from a website
#   are all included here.

require 'rubygems'
require 'phantomjs'
require 'watir-webdriver'
require 'csv'
require 'pp'

class WebScraper
	@browser
	@url
	@data
	@headers

	attr_accessor :data
	attr_accessor :headers

	def initialize( url, options = {} )
		if options.key?('browser')
			@browser = Watir::Browser.new options['browser']
		else
			@browser = Watir::Browser.new #Defaults to firefox
		end
		@browser.wait(timeout = 3)
		@browser.goto url
		@url = url
		# hash map/assoc. array of the Digi-key Partnumber (string) to attributes ( array of strings)
		#   e.g. { 'C8051F997-GMR-ND' => [C8051F997-GMR, Silicon Laboratories Inc, 8051, 8-Bit, ...]}
		#   this is where all data is stored during execution
		@data = {}
		@headers = {}
	end

	def scrape( options = {} )
	end

	def close
		@browser.close
	end

	def reset
		@browser.goto @url
	end

	def goto( url )
		@browser.goto url
	end

	# def enterParams( param, category )	
	# 	@browser.text_field( :name => 'KeyWords').set( param )
	# 	@browser.checkbox( :name => 'rohs' ).set
	# 	@browser.form( :name => 'Sform' ).submit
	# 	@browser.link( :text => category ).click
	# end

	# def enterQuantity( qty )
	# 	if @browser.link( :text => 'First' ).exists?
	# 		@browser.link( :text => 'First' ).click
	# 	end
	# 	@browser.text_field( :name => 'quantity' ).set ( qty )
	# 	@browser.button( :value => 'Submit' ).click
	# end

	# def readTable()
	# 	#if @headers.empty?
	# 	#	row = @browser.table( :id => 'productTable' ).row[0]
	# 	#	@headers = Hash[ row.map.with_index { |elem, idx| [elem, idx] }]
	# 	#	pp @headers
	# 	#end
	# 	table = @browser.table( :id => 'productTable' ).to_a

	# 	while @browser.link( :text => 'Next' ).exists? do
	# 		@browser.link( :text => 'Next' ).click
	# 		table += @browser.table( :id => 'productTable' ).to_a
	# 	end
	# 	len = table[0].length-1
	# 	hash = table.inject({}) {|h,e| h[e[3]] = e[4..len]; h};
	# 	@data.merge!( hash );
	# 	#return hash
	# end

	# # Expects a hash populated by readTable, then adds another price
	# def readPrice( qty, unit )
	# 	# Add to the header row
	# 	(@data['Digi-Key Part Number'] ||= []) << "Price @ #{unit}"
	# 	self.enterQuantity( qty )
	# 	# get number of results returned by digikey
	# 	count = @browser.p( :class => 'matching-records' ).text.scan(/\d+/).first.to_i
	# 	count -= 1

	# 	#pageSize = @browser.form( :class => 'page-dropdown' )

	# 	while true
	# 		# digikey returns 25 results per page
	# 		index = ( count > 24 ) ? 24 : count 
	# 		0.upto(index) do |i|
	# 			pn = @browser.td( :class => 'digikey-partnumber', :index => i ).text
	# 			price = @browser.td( :class => 'unitprice', :index => i ).text
	# 			( @data[pn] ||= [] ) << price
	# 		end

	# 		if @browser.link( :text => 'Next' ).exists?
	# 			@browser.link( :text => 'Next' ).click
	# 			count -= 24
	# 		else
	# 			break
	# 		end
	# 	end 
	# end
end
